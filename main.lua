-- Importing Libraries
require("libs/Lovetoys/src/engine")
require("libs/ShortLove/ShortLove")
flux = require "libs/flux/flux"

-- Imporitng Core
require("src/core/resources")
require("src/core/stackhelper")
require("src/core/state")

-- States
require("src/states/MenuState")

-- Utils
require("src/utils/saves")
require("src/utils/tables")

-- Data
require("data/settings")

function love.load()
   resources = Resources()

   -- Load Image 

   -- Load Sound

   -- Load Fonts
   resources:addFont("arial_48", "res/fonts/arial.ttf", 48)
   resources:addFont("arial_32", "res/fonts/arial.ttf", 32)

   resources:load()

   -- Load Animations

   -- Load Settings
   set = Settings()
   Saves():loadSettings()
   Saves():refreshSettings()

   -- Loading Stack
   stack = StackHelper()
   stack:push(MenuState())
end

function love.update(dt)
   stack:update(dt)
end

function love.draw()
   stack:draw()
end

function love.keypressed(key, isRepeat)
   stack:current():keypressed(key, isRepeat)
end

function love.mousepressed(x, y, button)
   stack:current():mousepressed(x, y, button)
end

function love.mousereleased(x, y, button)
   stack:current():mousereleased(x, y, button)
end

function love.quit()
   Saves:saveSettings()
   -- Add save stuff
end

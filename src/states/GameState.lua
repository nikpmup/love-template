-- Core Libraries
require("src/core/resources")
require("src/core/state")

-- Systems

-- Components

-- Events

-- States

GameState = class("GameState", State)

function GameState:load()

   -- Load LoveToys
   self.engine = Engine()
   self.eventManager = EventManager()

   -- Event Systems

   -- Event Handling

   -- Draw Systems

   -- Logic Systems

   -- Create Entities
end

function GameState:update(dt)
   self.engine:update(dt)
end

function GameState:draw()
   self.engine:draw()

   -- Resets drawing color
   lGfx.setColor(255, 255, 255, 255)
end

function GameState:keypressed(key, isRepeat)
end

function GameState:mousepressed(x, y, button)
end

function GameState:mousereleased(x, y, button)
end

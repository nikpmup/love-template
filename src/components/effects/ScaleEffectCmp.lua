ScaleEffectCmp = class("ScaleEffectCmp")

function ScaleEffectCmp:__init(minX, minY, maxX, maxY, minTime, maxTime)
   self.minX = minX
   self.minY = minY
   self.maxX = maxX
   self.maxY = maxY
   self.minTime = minTime
   self.maxTime = maxTime
end

SpriteModeCmp = class("SpriteModeCmp")

-- modes: single, loop
-- animCmp : Table of SpriteFrameCmp to run
function SpriteModeCmp:__init(mode, animTbl)
   self.mode = mode or "single"
   self.animTbl = animTbl
   self.curAnim = 1
end

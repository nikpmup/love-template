TextCmp = class("TextCmp")

function TextCmp:__init(font, color, string, values)
   self.font = font
   self.color = color
   self.string = string
   self.values = values or {}
end

SpriteFrameSys = class("SpriteFrameSys", System)

function SpriteFrameSys:update(dt)
   for i, entity in pairs(self.targets) do
      local spriteFrame = entity:get("SpriteFrameCmp")

      if spriteFrame.curDt > spriteFrame.animDt 
            and spriteFrame.curFrame < spriteFrame.maxFrame then
         local quadCmp = entity:get("QuadCmp")

         spriteFrame.curDt = spriteFrame.curDt - spriteFrame.animDt
         spriteFrame.curFrame = spriteFrame.curFrame + 1

         quadCmp.quad = spriteFrame.anim[spriteFrame.curFrame]
      end

      spriteFrame.curDt = spriteFrame.curDt + dt
   end
end

function SpriteFrameSys:requires()
   return { "SpriteFrameCmp", "QuadCmp" }
end

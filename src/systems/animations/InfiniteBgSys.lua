InfiniteBgSys = class("InfiniteBgSys", System)

function InfiniteBgSys:update(dt)
  for i, entity in pairs(self.targets) do
         local position = entity:get("PositionCmp")
         local scroll = entity:get("BgScrollCmp")

         position.x = position.x - dt * scroll.speed
         if position.x <= scroll.xMin then
            local dX = position.x - scroll.xMin
            position.x = scroll.xMax + dX 
         elseif position.x >= scroll.xMax then
            local dX = scroll.xMax - position.x
            position.x = scroll.xMax + dX
         end
   end
end

function InfiniteBgSys:requires()
   return { "PositionCmp", "BgScrollCmp" }
end

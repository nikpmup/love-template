RenderTextSys = class("RenderTextSys", System)

function RenderTextSys:draw()
   for i, entity in pairs(self.targets) do
      local str = entity:get("TextCmp")
      local pos = entity:get("PositionCmp")
      local align = entity:get("AlignCmp")
      local val = {} 

      for k, v in pairs(str.values) do
         table.insert(val, v[1][v[2]])
      end

      love.graphics.setColor(unpack(str.color))
      love.graphics.setFont(str.font)
      if (align ~= nil) then
         holder = unpack(val)
         if type(holder) == "table" then
            love.graphics.printf(string.format(str.string, unpack(holder)), 
               pos.x, pos.y, align.limit, align.alignment)
         else
            love.graphics.printf(string.format(str.string, unpack(val)), 
               pos.x, pos.y, align.limit, align.alignment)
         end
      else
         love.graphics.print(string.format(str.string, unpack(val)),
               pos.x, pos.y)
      end
   end
end

function RenderTextSys:requires()
   return { "PositionCmp", "TextCmp" }
end

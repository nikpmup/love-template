PhysicsSys = class("PhysicsSys", System)

function PhysicsSys:update(dt)
   for i, v in pairs(self.targets) do
      local physics = v:get("PhysicsCmp")
      local position = v:get("PositionCmp")

      -- Sync position
      position.x = physics.body:getX()
      position.y = physics.body:getY()
   end
end

function PhysicsSys:requires()
   return {"PhysicsCmp", "PositionCmp"}
end

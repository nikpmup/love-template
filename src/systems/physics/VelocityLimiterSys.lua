VelocityLimiterSys = class("VelocityLimiterSys", System)

function VelocityLimiterSys:update(dt)
   for i, v in pairs(self.targets) do
      local physics = v:get("PhysicsCmp")
      local limiter = v:get("VelocityLimiterCmp")
      local velX, velY = physics.body:getLinearVelocity()

      local sign
      if (math.abs(velX) < limiter.x) then
         sign = velX < 0 and -1 or 1
         velX = sign * limiter.x
      end

      if (math.abs(velY) < limiter.y) then
         sign = velY < 0 and -1 or 1
         velY = sign * limiter.y
      end

      physics.body:setLinearVelocity(velX, velY)
   end
end

function VelocityLimiterSys:requires()
   return { "PhysicsCmp", "VelocityLimiterCmp" }
end
